% Algorithmique en première NSI
% UCBL
% 20 Octobre 2020

# Le programme de première NSI


## Partie 1


!["Partie 1"](images/progNSI1.png "autre")

## Partie 2

!["Partie 2"](images/progNSI2.png "autre")

## Partie 3

!["Partie 3"](images/progNSI3.png "autre")

# Point de départ de l'activité

## La source 

Chapitre 1.4 Analysis of algorithms Robert Sedgewick / Kevin Wayne_

<https://algs4.cs.princeton.edu/14analysis/>

## Les questions

>_How long will my program take?_

>_Why does my program run out of memory ?_

## Scientific Method (p. 172)

* _Observe_
* _Hypothesize_
* _Predict_
* _Verify_
* _Validate_

# Partie 1 de l'activité

## Question 1

On peut mettre l'archive zip à disposition des élèves sur le réseau pédagogique pour éviter les téléchargments, ou l'ENT.

Consignes données : extraire l'archive dans un répertoire pertinent du  répertoire NSI de leur espace personnel sur le réseau pédagogique.

## Question 2 (1/2)

~~~python
def extraire(fichier):
    """Renvoie le contenu d'un fichier texte avec un entier par ligne
    sous la forme d'un tableau"""
    f = open(fichier)
    tab = [int(ligne) for ligne in f]
    f.close()
    return tab
~~~

## Question 2 (2/2)

~~~python
def deux_sommes(tab):
    """Renvoie un couple :
    - le nombre de couples d'entiers dont la somme est égale à 0 dans le tableau d'entiers tab
    - le temps d'exécution
    """
    n = len(tab)
    debut = time.perf_counter()
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if tab[i] + tab[j] == 0:
                c = c + 1
    fin = time.perf_counter()
    return (c, fin - debut)


assert deux_sommes(tab_1K)[0] == 1
assert deux_sommes(tab_2K)[0] == 2
assert deux_sommes(tab_4K)[0] == 3
~~~

## Question 3 (1/2)

On peut demander aux élèves d'écrire une fonction renvoyant la somme des éléments d'un tableau passé en paramètre et leur fournir une postcondition avec un `assert`.

## Question 3 (2/2)

~~~python
tab_1K = extraire('1Kints.txt')
tab_2K = extraire('2Kints.txt')
tab_4K = extraire('4Kints.txt')

def somme(tab):
    """Renvoie la somme des éléments du tableau tab"""
    s = 0
    for e in tab:
        s = s + e
    return s

assert (somme(tab_1K), somme(tab_2K), somme(tab_4K)) == (1277931, 28383875, 6388500)
~~~

## Question 4 (1/2)

Code élève :

~~~python
def deux_sommes(tab):
   	n = len(tab)
    debut = time.perf_counter()
    c = 0
    temps = debut
    for i in range(n):
        for j in range(n):
            if tab[i] + tab[j] == 0:
                c += 1
    			temps += time.perf_counter()
    	return (c, temps)
~~~


## Question 4 (2/2)

Remédiation :

~~~python
def deux_sommes(tab):
   	n = len(tab)
    debut = time.perf_counter()
    c = 0
    temps = debut
    for i in range(n):        
        for j in range(n):
            #Déroulez le programme à la main sur un petit tableau de taille 4
            #Affichez le couple (i, j) ici
            if tab[i] + tab[j] == 0:
                c += 1
                #relisez la documentation de time.perf_counter()
                #comment chronométrez-vous une course ?
    			temps += time.perf_counter()
        #Déroulez le programme à la main sur un petit tableau de taille 4
        #Que se passe-t-il à la fin du premier tour de boucle ?
    	return (c, temps)
~~~


## Question 5


~~~python
def trois_sommes(tab):
    """Renvoie un couple :
    - le nombre de triplets d'entiers dont la somme est égale à 0 dans le tableau d'entiers tab
    - le temps d'exécution
    """
    n = len(tab)
    debut = time.perf_counter()
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                if tab[i] + tab[j] + tab[k] == 0:
                    c = c + 1
    return (c, time.perf_counter() - debut)

#assert trois_sommes(tab_1K)[0] == 70
#assert trois_sommes(tab_2K)[0] == 528
#assert trois_sommes(tab_4K)[0] == 4039
~~~


## Question 6 : Diversité des langages (1/3)

~~~java
public class ThreeSum {

     public static int count(int[] a) {
        int N = a.length;
        int cnt = 0;
        for (int i = 0; i < N; i++) {
            for (int j = i+1; j < N; j++) {
                for (int k = j+1; k < N; k++) {
                    if (a[i] + a[j] + a[k] == 0) {
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    } 

    public static void main(String[] args)  { 
        int[] a = In.readInts(args[0]);
        Stopwatch timer = new Stopwatch();
        int cnt = count(a);
        StdOut.println("elapsed time = " + timer.elapsedTime());
        StdOut.println(cnt);
    } 
}
~~~

## Question 6 : Diversité des langages (2/3)

~~~bash
fjunier@fjunier:~$ java ThreeSum 1Kints.txt 
elapsed time = 0.308
70
fjunier@fjunier:~$ java ThreeSum 2Kints.txt 
elapsed time = 2.37
528
fjunier@fjunier:~$ java ThreeSum 4Kints.txt 
elapsed time = 18.97
4039
~~~


## Question 6 (3/3)

On peut théoriquement décrire le temps d'exécution d'un programme, selon deux critères :

* le coût d'exécution de chaque instruction (dépend de la plateforme (processeur, OS), du compilateur / interpréteur donc du langage d'où les différences entre Python et Java)
* la fréquence d'exécution de chaque instruction (c'est une propriété de l'algorithme  et de l'entrée du programme qui ne dépend pas du langage)

En multipliant les deux et en ajoutant pour chaque instruction, on doit obtenir une estimation du temps d'exécution du programme.


## Question 7 (1/4)


On commence par faire écrire une fonction `generer_tab_aleatoire` pour générer des tableaux aléatoires.

~~~python
from random import randint

def generer_tab_aleatoire(n, binf, bsup):
    """Renvoie un tableau de taille n
    d'entiers aléatoires entre binf et bsup"""
    return [random.randint(binf, bsup) for _ in range(n)]
~~~

## Question 7 (2/4)

~~~python
def testDoublement(nstart, nbiter, recherche, debug = True):
    """
    Paramètres :
        - nstart de type int taille du tableau initial
        - nbiter de type int nombre d'itérations
        - recherche de type function désigne la fonction de recherche (paradigme fonctionnel ?)
    Valeur renvoyée : 
        - un tableau ttemps des temps de recherche pour chaque tableau
        - un tableau ttailler des tailles des tableaux successifs 
    """
    n = nstart
    ttaille = []
    ttemps = []
    MAX = 1000000
    for _ in range(nbiter):
        tab =  generer_tab_aleatoire(n, -MAX, MAX)
        #on peut demander de compléter le reste du corps de boucle
        compte, temps = recherche(tab)
        ttaille.append(n)
        ttemps.append(temps)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s")
        n = n * 2        
    return ttemps, ttaille   
~~~

## Question 7 (3/4)

Avec un `while`.

~~~python
def testDoublementWhile(nstart, recherche, temps_max, debug = True):
    n = nstart
    ttaille = []
    ttemps = []
    temps = 0
    MAX = 1000000
    while temps < temps_max:
        tab =  generer_tab_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ttaille.append(n)
        ttemps.append(temps)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s")
        n = n * 2        
    return ttemps, ttaille       
~~~

## Question 7 (4/4)

Un exemple de sortie :

~~~bash
Taille = 1 --> Temps = 1.775e-06 s
Taille = 2 --> Temps = 2.540e-06 s
Taille = 4 --> Temps = 5.227e-06 s
Taille = 8 --> Temps = 2.745e-05 s
Taille = 16 --> Temps = 1.219e-04 s
Taille = 32 --> Temps = 8.264e-04 s
Taille = 64 --> Temps = 6.240e-03 s
Taille = 128 --> Temps = 5.481e-02 s
Taille = 256 --> Temps = 3.943e-01 s
Taille = 512 --> Temps = 3.407e+00 s
Taille = 1024 --> Temps = 2.744e+01 s
~~~


## Question 8 (1/3)

Le programme `DoublingRatio` en Java. 

~~~java
public class DoublingRatio {

    public static double timeTrial(int N) {
        int MAX = 1000000;
        int[] a = new int[N];
        for (int i = 0; i < N; i++) {
            a[i] = StdRandom.uniform(-MAX, MAX);
        }
        Stopwatch s = new Stopwatch();
        int cnt = ThreeSum.count(a);
        return s.elapsedTime();
    }


    public static void main(String[] args) { 
        double prev = timeTrial(125);
        for (int N = 250; true; N += N) {
            double time = timeTrial(N);
            StdOut.printf("%6d %7.1f %5.1f\n", N, time, time/prev);
            prev = time;
        } 
    } 
} 
~~~

## Question 8 (2/3)

On peut observer  avec les élèves :

* les différences entre  Python et Java :
  * Le typage statique en Java
  * Une fonction `main` en Java pour le programme principal
  * Les accolades pour délimiter les blocs en Java
  * La verbosité de Java

## Question 8 (2,5/3)

* les points communs entre Python et Java : 
  * le = pour l'affectation
  * des mots clefs comme`for`
  * l'accès aux éléments d'un tableau
  * l'utilisation de fonctions


## Question 8 (3/3)

~~~python
def testDoublementRatio(nstart, nbiter, recherche, debug = True):
    MAX = 1000000
    n = nstart
    ttaille = []
    ttemps = []
    tratio = []
    tab =  generer_tab_aleatoire(n, -MAX, MAX)
    compte, temps = recherche(tab)
    ttaille.append(n)
    ttemps.append(temps)    
    for k in range(nbiter - 1):
        n = n * 2 
        tab =  generer_tab_aleatoire(n, -MAX, MAX)
        #on peut demander de compléter le reste du corps de boucle
        compte, temps = recherche(tab)
        ratio = temps/ttemps[k]
        ttaille.append(n)
        ttemps.append(temps)
        tratio.append(ratio)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s --> Ratio = {ratio:.3e}")             
    return ttemps, ttaille, tratio  
~~~


## Question 9

La valeur `Infinity` est due à une division par zéro due au temps d'exécution très faible
et assimilé à 0 de  `timeTrial(125)`  pour `TwoSum`. Il suffit de choisir une taille plus importante
pour le tableau initiale, par exemple 500.

## Question 10 (1/4)

On peut leur proposer de remplir ce tableau pour les amener à conjecturer les bonnes lois de puissances en $n^3$ pour `trois_sommes` et $n^2$ pour `deux_sommes` :

| n 	| $n^2$ 	|  $n^3$ 	|
|---	|-----	|------	|
| 1 	| 1   	| 1    	|
| 2 	| 4   	| 8    	|
| 4 	| 16  	| 64   	|
| 8 	| 64  	| 512  	|

## Question 10 (2/4)

Pour les calculs de fréquence d'exécution du bloc de la boucle interne, on peut poser des questions à partir d'un contexte comme une course de 8 chevaux : 

* combien de  possibilités pour les deux premiers  dans l'ordre ? et dans le désordre ?
* combien de  possibilités pour le tiercé  dans l'ordre ? et dans le désordre ?

## Question 10 (3/4)

On pourra projeter cette capture d'écran du manuel _Algorithms_  page 181 :

![Anatomy of a program's stement execution frequencies](images/anatomy_of_frequencies.png)

## Question 10 (4/4)

Puis ce calcul plus détaillé :

![Anatomy of a program's stement execution frequencies](images/anatomy_of_frequencies2.png)


# Partie 2

## Question 1

~~~python
def recherche_seq(tab, element):
    """Recherche séquentielle d'un élément dans un tableau :
    - Renvoie  l'index de la première occurence
    - ou -1 si l'élément n'est pas dans le tableau
    """
    for k in range(len(tab)):
        if tab[k] == element:
            return k
    return -1

def deux_sommes2(tab):
    """Renvoie un couple :
    - le nombre de couples d'entiers dont la somme est égale à 0 dans le tableau d'entiers tab
    - le temps d'exécution
    Précondition : tous les éléments de tab doivent être distincts
    """
    assert tous_distincts(tab)
    n = len(tab)
    debut = time.perf_counter()
    c = 0
    for i in range(n):
        if recherche_seq(tab, -tab[i]) > i:
            c = c + 1
    fin = time.perf_counter()
    return (c, fin - debut)
~~~

## Question 2

~~~python
def tous_distincts(tab):
    """Fonction qui renvoie un booléen 
    indiquant si tous les éléments de tab sont distincts"""
    n = len(tab)
    for i in range(n):
        for j in range(i + 1, n):
            if tab[i] == tab[j]:
                return False
    return True

assert tous_distincts(list(range(1, 100, random.randint(1, 10))))
assert not tous_distincts([random.randint(1, 6) for _ in range(20)])

def generer_tab_aleatoire_distincts(n, binf, bsup):
    """Renvoie un tableau de taille n qui est un échantillon
    d'entiers aléatoires entre binf et bsup
    Postcondition : tous les éléments doivent être distincts"""
    urne = list(range(binf, bsup + 1))
    echantillon = []
    for _ in range(n):
        choix = urne.pop(random.randint(0, len(urne) - 1))
        echantillon.append(choix)
    return echantillon

for n in [10, 50, 100, 1000]:
    assert tous_distincts(generer_tab_aleatoire_distincts(100, -1000, 1000))
~~~


## Question 3 (1/3)

~~~python
def testDoublement2(nstart, nbiter, recherche, generateur_aleatoire, debug = True):
    n = nstart
    ttaille = []
    ttemps = []
    MAX = 1000000
    for _ in range(nbiter):
        tab =  generateur_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ttaille.append(n)
        ttemps.append(temps)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s")
        n = n * 2        
    return ttemps, ttaille   


def testDoublementRatio2(nstart, nbiter, recherche,  generateur_aleatoire, debug = True):
    MAX = 1000000
    n = nstart
    ttaille = []
    ttemps = []
    tratio = []
    tab =  generateur_aleatoire(n, -MAX, MAX)
    compte, temps = recherche(tab)
    ttaille.append(n)
    ttemps.append(temps)    
    for k in range(nbiter - 1):
        n = n * 2 
        tab =  generateur_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ratio = temps/ttemps[k]
        ttaille.append(n)
        ttemps.append(temps)
        tratio.append(ratio)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s --> Ratio = {ratio:.3e}")    
~~~


## Question 3 (2/3)

On peut remarquer que sur des tableaux aléatoires de même taille les temps d'exécution de `deux_sommes2` et `deux_sommes` sont du même ordre de grandeur mais que `deux_sommes` est majoritairement plus rapide sur l'échantillon proposé.

## Question 3 (3/3)


En examinant les boucles internes de `deux_sommes2`  et  `deux_sommes` on peut remarquer :

* qu'au tour de boucle externe d'index `i`,  `deux_sommes` exécute `n - i - 1` tours soit $(1+2+\ldots+n-1)/n=n/2$ en moyenne
* tandis que `deux_sommes2` exécute entre 1 et `n` tours soit $(n+1)/2$ en moyenne sur des données aléatoires (mais la fonction de génération de tableau aléatoire le garantit-elle ?).



## Question 4

On pourra insister sur la précondition de tri du tableau pour lui appliquer une recherche dichotomique. 

Pour une fonction de tri, on pourra distinguer le tri en place par une procédure  (`tab.sort()`) ou le tri externe  par une fonction (`sorted(tab)`). On pourra rappeler que les tris par sélection et insertion ont un coût temporel quadratique par rapport à la taille du tableau dans le pire des cas mais que les fonctions de bibliothèques comme la méthode `sort` ou la fonction `sorted` sont plus efficaces.



## Question 5 (1/7)

La _complexité temporelle_ donne un ordre de grandeur du temps d'exécution  d'un algorithme en général en  fonction de la taille de l'entrée. Cet ordre de grandeur ne dépend pas de l'implémentation (langage, machine) mais d'un modèle de coût qui définit les opérations de base utilisées par l'algorithme : par exemple le nombre d'accès tableau pour les fonctions `troi_sommes` et `deux_sommes`.

## Question 5 (2/7)

Citation de Robert Sedgewick  :

>The idea tat the order of growth of the running time of ThreeSum est $N^3$ does not depend on the fact it is implemented in Java or that is running on your laptop or someone's else cellphone or a supercomputer.


## Question 5 (3/7)

Citation de Robert Sedgewick  :

>Our intent is to articulate cost models such that the order of growth of the running time for a given implementation is the same as the order of growth of the underlying algorithm (in other words, the cost model should include operations that fall within the inner loop).

## Question 5 (4/7)

Analyse de complexité pour les tris au programme, avec comme modèle de coût le nombre de comparaisons et comme entrée la taille du tableau :

Pour le _tri par sélection_ : on effectue toujours une recherche du plus petit élément parmi $n-1$ puis $n-2$ ... puis $1$ éléments non triés donc le coût en comparaisons est toujours de : $$1+2+...+n-1=n(n-1)/2$$

## Question 5 (5/7)

Pour le _tri par insertion_ : dans le pire des cas (tableau trié dans l'ordre inverse), on effectue $1$ puis $2$ .... puis $n-1$ décalages pour insérer  donc le coût en comparaisons est alors  de :
$$1+2+...+n-1=n(n-1)/2$$

## Question 5 (6/7)

Analyse de complexité  pour la recherche dichotomique dans un tableau trié avec comme modèle de coût le nombre de comparaisons et comme entrée la taille du tableau :

Notons $C(N)$ le coût pour un tableau de taille $N$.

La condition dans la boucle interne se traduit par la relation de récurrence :
$C(N) \leqslant C(\lfloor N/2 \rfloor) + 1$

Pour simplifier on pose $N=2^{n} - 1$,

donc  $\lfloor N/2 \rfloor=2^{n-1}-1$ et  $C(N) \leqslant C(2^{n-1}-1) + 1$

## Question 5 (7/7)

En itérant $n-1$ fois  la relation de récurrence, il vient :

$C(N) \leqslant C(2^{n-2}-1)+2$ puis ..... $C(N) \leqslant C(2^{n-(n-1)}-1)+n - 1$

Donc $C(N) \leqslant C(1)+n -1$

Or $C(1)=1$ donc $C(N)=C(2^{n}-1)$ majoré par $n$ qui n'est autre que le nombre de chiffres de $N$ en base $2$. Si on note $n=\log_{2}(2^{n})$ alors $C(N) \leqslant n = 1 + \lfloor \log_{2}(N) \rfloor$

## Question 6 (1/4)

Pour `deux_sommes`  la recherche dichotomique n'est  efficace que si on utilise une fonction de tri de bibliothèque optimale en complexité  : 

* En effet, le coût en $N^{2}$ des tris par insertion ou par sélection va dominer le coût en $N \log_{2}(N)$ de la boucle de recherche.

* Avec un tri de bibilothèque en  $N \log_{2}(N)$, le coût est le même que pour la boucle de recherche donc $N \log_{2}(N)$ au total, on parle de coût linéarithmique.


## Question 6 (2/4)


~~~python
def deux_sommes_rapide(tab):
    """Renvoie le nombre de couples d'entiers dans un tableau
    dont la somme est égale à 0
    Précondition : tous les entiers sont distints
    Complexité en O(n²) coût dominant du tri quadratique
    """
    n = len(tab)
    debut = time.perf_counter()
    tri_insertion(tab)
    c = 0
    for i in range(n):
        if recherche_dicho(tab, -tab[i]) > i:
            c = c + 1
    return (c, time.perf_counter() - debut)

def deux_sommes_rapide2(tab):
    """Renvoie le nombre de couples d'entiers dans un tableau
    dont la somme est égale à 0
    Précondition : tous les entiers sont distints
    Complexité en O(nlog(n)) 
    """
    n = len(tab)
    debut = time.perf_counter()
    tab.sort()
    c = 0
    for i in range(n):
        if recherche_dicho(tab, -tab[i]) > i:
            c = c + 1
    return (c, time.perf_counter() - debut)
~~~


## Question 6 (3/4)

Pour `3_sommes` le coût de la boucle en $N^{2} \log_{2}(N)$ est dominant sur le tri qu'il soit quadratique ou de bibliothèque en $N \log_{2}(N)$.

On peut à cette occasion mettre en évidence le rôle des facteurs dominants dans l'ordre de grandeur de la complexité, du coût temporel (fixer le vocabulaire ?) d'un algorithme.

## Question 6 (4/4)

~~~python
def trois_sommes_rapide(tab):
    """Renvoie le nombre de triplets d'entiers dans un tableau
    dont la somme est égale à 0.
    Précondition : tous les entiers sont distints
    Complexité en O(N^2 log(N))
    """
    n = len(tab)
    debut = time.perf_counter()
    tri_insertion(tab)
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if recherche_dicho(tab, -(tab[i] + tab[j])) > j:
                    c = c + 1
    return (c, time.perf_counter() - debut)


def trois_sommes_rapide2(tab):
    """Renvoie le nombre de triplets d'entiers dans un tableau
    dont la somme est égale à 0.
    Précondition : tous les entiers sont distints
    Complexité en O(N^2 log(N))
    """
    n = len(tab)
    debut = time.perf_counter()
    tab.sort()
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if recherche_dicho(tab, -(tab[i] + tab[j])) > j:
                    c = c + 1
    return (c, time.perf_counter() - debut)
~~~


