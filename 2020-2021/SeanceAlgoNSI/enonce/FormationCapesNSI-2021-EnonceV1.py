#!/usr/bin/env python
# coding: utf-8

# In[1]:


import time
import random
import matplotlib.pyplot as plt


# ## Partie 1 : algorithmes naifs

# In[2]:


def extraire(fichier):
    """Renvoie le contenu d'un fichier texte avec un entier par ligne
    sous la forme d'un tableau"""
    f = open(fichier)
    tab = [int(ligne) for ligne in f]
    f.close()
    return tab


# In[3]:


def somme(tab):
    """Renvoie la somme des éléments du tableau tab"""
    s = 0
    for e in tab:
        s = s + e
    return s


# In[4]:


tab_1K = extraire('1Kints.txt')
tab_2K = extraire('2Kints.txt')
tab_4K = extraire('4Kints.txt')


# In[5]:


somme(tab_1K), somme(tab_2K), somme(tab_4K)


# In[6]:


assert (somme(tab_1K), somme(tab_2K), somme(tab_4K)) == (1277931, 28383875, 6388500)


# In[16]:


help(time.perf_counter)


# In[45]:


def deux_sommes(tab):
    """Renvoie un couple :
    - le nombre de couples d'entiers dont la somme est égale à 0 dans le tableau d'entiers tab
    - le temps d'exécution
    """
    n = len(tab)
    debut = time.perf_counter()
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if tab[i] + tab[j] == 0:
                c = c + 1
    fin = time.perf_counter()
    return (c, fin - debut)


assert deux_sommes(tab_1K)[0] == 1
assert deux_sommes(tab_2K)[0] == 2
assert deux_sommes(tab_4K)[0] == 3


# In[48]:


def trois_sommes(tab):
    """Renvoie un couple :
    - le nombre de triplets d'entiers dont la somme est égale à 0 dans le tableau d'entiers tab
    - le temps d'exécution
    """
    n = len(tab)
    debut = time.perf_counter()
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                if tab[i] + tab[j] + tab[k] == 0:
                    c = c + 1
    return (c, time.perf_counter() - debut)

#assert trois_sommes(tab_1K)[0] == 70
#assert trois_sommes(tab_2K)[0] == 528
#assert trois_sommes(tab_4K)[0] == 4039


# In[28]:


trois_sommes(tab_1K)


# In[24]:


tab_1K = extraire('1Kints.txt')
tab_2K = extraire('2Kints.txt')
tab_4K = extraire('4Kints.txt')


# In[50]:


deux_sommes(tab_1K)


# In[51]:


deux_sommes(tab_2K)


# In[52]:


deux_sommes(tab_4K)


# ~~~
# fjunier@fjunier:~/NSI/CapesNSI/Formation-2020-2021$ java TwoSum 1Kints.txt 
# elapsed time = 0.005
# 1
# fjunier@fjunier:~/NSI/CapesNSI/Formation-2020-2021$ java TwoSum 2Kints.txt 
# elapsed time = 0.012
# 2
# fjunier@fjunier:~/NSI/CapesNSI/Formation-2020-2021$ java TwoSum 4Kints.txt 
# elapsed time = 0.028
# 3
# ~~~

# In[21]:


trois_sommes(tab_1K)


# In[22]:


trois_sommes(tab_2K)


# In[ ]:


trois_sommes(tab_4K)


# In[5]:


def generer_tab_aleatoire(n, binf, bsup):
    """Renvoie un tableau de taille n
    d'entiers aléatoires entre binf et bsup"""
    return [random.randint(binf, bsup) for _ in range(n)]    


# In[6]:


def testDoublement(nstart, nbiter, recherche, debug = True):
    n = nstart
    ttaille = []
    ttemps = []
    MAX = 1000000
    for _ in range(nbiter):
        tab =  generer_tab_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ttaille.append(n)
        ttemps.append(temps)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s")
        n = n * 2        
    return ttemps, ttaille   


def testDoublementWhile(nstart, recherche, temps_max, debug = True):
    n = nstart
    ttaille = []
    ttemps = []
    temps = 0
    MAX = 1000000
    while temps < temps_max:
        tab =  generer_tab_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ttaille.append(n)
        ttemps.append(temps)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s")
        n = n * 2        
    return ttemps, ttaille        


# In[11]:


ttemps, ttaille = testDoublement(1, 11, trois_sommes)


# In[114]:


get_ipython().run_line_magic('matplotlib', 'inline')

def graphique(tx,ty, fichier):
    plt.xlabel('Taille du tableau')
    plt.ylabel('Temps en secondes')
    plt.plot(tx, ty, 'o')
    plt.savefig(fichier)
    plt.show()
    
graphique(ttaille, ttemps, 'testDoublement.png')


# In[65]:


ttemps, ttaille = testDoublement(1, 11, deux_sommes)


# In[66]:


get_ipython().run_line_magic('matplotlib', 'inline')

def graphique(tx,ty, fichier):
    plt.plot(tx, ty, 'o')
    plt.savefig(fichier)
    plt.show()
    
graphique(ttaille, ttemps, 'testDoublement.png')


# In[12]:


def testDoublementRatio(nstart, nbiter, recherche, debug = True):
    MAX = 1000000
    n = nstart
    ttaille = []
    ttemps = []
    tratio = []
    tab =  generer_tab_aleatoire(n, -MAX, MAX)
    compte, temps = recherche(tab)
    ttaille.append(n)
    ttemps.append(temps)    
    for k in range(nbiter - 1):
        n = n * 2 
        tab =  generer_tab_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ratio = temps/ttemps[k]
        ttaille.append(n)
        ttemps.append(temps)
        tratio.append(ratio)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s --> Ratio = {ratio:.3e}")             
    return ttemps, ttaille, tratio  


# In[15]:


ttemps, ttaille, tratio = testDoublementRatio(1, 11, trois_sommes)


# Vérification que temps ~ taille$^{3}$ pour `trois_sommes`

# In[17]:


[temps / taille ** 3 for (temps, taille) in zip(ttemps, ttaille)]


# In[20]:


ttemps, ttaille, tratio = testDoublementRatio(1, 14, deux_sommes)


# Vérification que temps ~ taille$^{2}$ pour `deux_sommes`

# In[21]:


[temps / taille ** 2 for (temps, taille) in zip(ttemps, ttaille)]


# ## Tests avec Java
# 
# Sitographie :
# 
# * Librairies : <https://introcs.cs.princeton.edu/java/stdlib/>
# * Programmes : <https://algs4.cs.princeton.edu/14analysis/>
# 
# ~~~
# fjunier@fjunier:~/NSI/CapesNSI/Formation-2020-2021$ java DoublingTest
#     250   0.0
#     500   0.0
#    1000   0.3
#    2000   2.3
#    4000  18.7
#    8000 152.3
# ~~~
# 
# ~~~
# fjunier@fjunier:~/NSI/CapesNSI/Formation-2020-2021$ java DoublingRatio
#    250     0.0   1.4
#    500     0.0   5.4
#   1000     0.3   7.6
#   2000     2.3   7.9
#   4000    18.2   8.0
# ~~~

# ## Partie 2 : algorithmes avec recherche dichotomique
# 
# 
# Pour simplifier on suppose que tous les éléments sont distincts

# In[31]:


def tous_distincts(tab):
    """Fonction qui renvoie un booléen 
    indiquant si tous les éléments de tab sont distincts"""
    n = len(tab)
    for i in range(n):
        for j in range(i + 1, n):
            if tab[i] == tab[j]:
                return False
    return True

assert tous_distincts(list(range(1, 100, random.randint(1, 10))))
assert not tous_distincts([random.randint(1, 6) for _ in range(20)])

def generer_tab_aleatoire_distincts(n, binf, bsup):
    """Renvoie un tableau de taille n qui est un échantillon
    d'entiers aléatoires entre binf et bsup
    Postcondition : tous les éléments doivent être distincts"""
    urne = list(range(binf, bsup + 1))
    echantillon = []
    for _ in range(n):
        choix = urne.pop(random.randint(0, len(urne) - 1))
        echantillon.append(choix)
    return echantillon

for n in [10, 50, 100, 1000]:
    assert tous_distincts(generer_tab_aleatoire_distincts(100, -1000, 1000))


# In[38]:


def testDoublement2(nstart, nbiter, recherche, generateur_aleatoire, debug = True):
    n = nstart
    ttaille = []
    ttemps = []
    MAX = 1000000
    for _ in range(nbiter):
        tab =  generateur_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ttaille.append(n)
        ttemps.append(temps)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s")
        n = n * 2        
    return ttemps, ttaille   


def testDoublementRatio2(nstart, nbiter, recherche,  generateur_aleatoire, debug = True):
    MAX = 1000000
    n = nstart
    ttaille = []
    ttemps = []
    tratio = []
    tab =  generateur_aleatoire(n, -MAX, MAX)
    compte, temps = recherche(tab)
    ttaille.append(n)
    ttemps.append(temps)    
    for k in range(nbiter - 1):
        n = n * 2 
        tab =  generateur_aleatoire(n, -MAX, MAX)
        compte, temps = recherche(tab)
        ratio = temps/ttemps[k]
        ttaille.append(n)
        ttemps.append(temps)
        tratio.append(ratio)
        if debug:
            print(f"Taille = {n} --> Temps = {temps:.3e} s --> Ratio = {ratio:.3e}")             
    return ttemps, ttaille, tratio  


# In[42]:


ttemps, ttaille = testDoublement2(1, 14, deux_sommes2, generer_tab_aleatoire_distincts)


# In[43]:


ttemps, ttaille, tratio = testDoublementRatio2(1, 14, deux_sommes2,  generer_tab_aleatoire_distincts)


# In[49]:


ttemps, ttaille, tratio = testDoublementRatio2(1, 14, deux_sommes,  generer_tab_aleatoire)


# In[20]:


def recherche_seq(tab, element):
    """Recherche séquentielle d'un élément dans un tableau :
    - Renvoie  l'index de la première occurence
    - ou -1 si l'élément n'est pas dans le tableau
    """
    for k in range(len(tab)):
        if tab[k] == element:
            return k
    return -1

def deux_sommes2(tab):
    """Renvoie un couple :
    - le nombre de couples d'entiers dont la somme est égale à 0 dans le tableau d'entiers tab
    - le temps d'exécution
    Précondition : tous les éléments de tab doivent être distincts
    """
    n = len(tab)
    debut = time.perf_counter()
    c = 0
    for i in range(n):
        if recherche_seq(tab, -tab[i]) > i:
            c = c + 1
    fin = time.perf_counter()
    return (c, fin - debut)


# In[22]:


for tab in [tab_1K, tab_2K, tab_4K]:
    assert tous_distincts(tab)


# In[55]:


def recherche_dicho(tab, element):
    """Recherche l'entier element dans le tableau d'entiers tab
    Précondition : tab trié dans l'ordre croissant
    Si element est dans tab renvoie son index
    Sinon renvoie -1"""
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        med = (debut + fin) // 2
        if element < tab[med]:
            fin = med - 1
        elif element > tab[med]:
            debut = med + 1
        else:
            return med
    return -1


# In[54]:


def tri_selection(tab):
    """Tri par sélection en place du tableau tab"""
    n = len(tab)
    for i in range(n - 1):
        imin = i
        for j in range(i + 1, n):
            if t[j] < t[imin]:
                imin = j
        t[i], t[imin] = t[imin], t[i]


# In[52]:


def tri_insertion(tab):
    """Tri par insertion en place du tableau tab"""
    n = len(tab)
    for i in range(1, n):
        j = i - 1
        element = tab[i]
        while j >= 0 and tab[j] > element:
            tab[j+1] = tab[j]
            j =  j - 1
        tab[j + 1] = element


# In[51]:


def deux_sommes_rapide(tab):
    """Renvoie le nombre de couples d'entiers dans un tableau
    dont la somme est égale à 0
    Précondition : tous les entiers sont distints"""
    n = len(tab)
    debut = time.perf_counter()
    tri_insertion(tab)
    c = 0
    for i in range(n):
        if recherche_dicho(tab, -tab[i]) > i:
            c = c + 1
    return (c, time.perf_counter() - debut)


# In[56]:


ttemps, ttaille = testDoublement2(1, 15, deux_sommes_rapide, generer_tab_aleatoire_distincts)


# In[59]:


ttemps, ttaille, tratio = testDoublementRatio2(1, 15, deux_sommes_rapide,  generer_tab_aleatoire)


# In[61]:


def deux_sommes_rapide2(tab):
    """Renvoie le nombre de couples d'entiers dans un tableau
    dont la somme est égale à 0
    Précondition : tous les entiers sont distints"""
    n = len(tab)
    debut = time.perf_counter()
    tab.sort()
    c = 0
    for i in range(n):
        if recherche_dicho(tab, -tab[i]) > i:
            c = c + 1
    return (c, time.perf_counter() - debut)


# In[62]:


ttemps, ttaille = testDoublement2(1, 15, deux_sommes_rapide2, generer_tab_aleatoire_distincts)


# In[64]:


ttemps, ttaille, tratio = testDoublementRatio2(1, 15, deux_sommes_rapide2,  generer_tab_aleatoire_distincts)


# In[66]:


def trois_sommes_rapide(tab):
    """Renvoie le nombre de triplets d'entiers dans un tableau
    dont la somme est égale à 0.
    Précondition : tous les entiers sont distints"""
    n = len(tab)
    debut = time.perf_counter()
    tri_insertion(tab)
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if recherche_dicho(tab, -(tab[i] + tab[j])) > j:
                    c = c + 1
    return (c, time.perf_counter() - debut)


# In[67]:


def trois_sommes_rapide2(tab):
    """Renvoie le nombre de triplets d'entiers dans un tableau
    dont la somme est égale à 0.
    Précondition : tous les entiers sont distints"""
    n = len(tab)
    debut = time.perf_counter()
    tab.sort()
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if recherche_dicho(tab, -(tab[i] + tab[j])) > j:
                    c = c + 1
    return (c, time.perf_counter() - debut)


# In[69]:


ttemps, ttaille, tratio = testDoublementRatio2(1, 12, trois_sommes_rapide,  generer_tab_aleatoire_distincts)


# In[71]:


ttemps, ttaille = testDoublement2(1, 12, trois_sommes_rapide2,  generer_tab_aleatoire_distincts)

