---
title : Séance Données personnelles, éléments de correction
author : Frédéric Junier
numbersections: true
fontsize: 11pt
geometry:
- top=20mm
- left=20mm
- right=20mm
- heightrounded    
--- 



# Sujet 1

## Question 1 :


1. Premier temps : Situation d'accroche

* Proposer un challenge proposé également dans Pix :  retrouver la commune où a été prise cette photo : <http://frederic-junier.org/SNT/images/20181230_162625.jpg>. On pourra utiliser l'outil en ligne <http://exif-viewer.com/>    pour éditer les metadonnées de la photo ou le plugin Firefox <https://addons.mozilla.org/fr/firefox/addon/exif-viewer/>  ou [exif-tool](https://exiftool.org/) en version portable.
* Compléter avec une manipulation des paramètres de géolocalisation sur l'ordiphone de chaque élève (activation du GPS, paramétrage de autorisations d'application)

2. Second temps : Définir les notions étudiées
   
* Proposer des activités de découverte des concepts (Métadonnées, Géolocalisation) sur le manuel  :
  *  les metadonnées Exif : 
     * Activité de découverte comme celle page 111 du manuel Delagrave :  
     ![données exif](images/exif.png "alt")\
     * Exercice d'application comme l'exercice 6 page 119 du manuel Delagrave :  
     ![données exif2](images/exif2.png "alt")\
  * le principe de la  géolocalisation par GPS sur un ordiphone par exemple :  
    * Activité 2 pages 82 et 83 du manuel de SNT Delagrave :
  
      ![données exif2](images/gps.png "alt")\


1. Troisième temps : Débat

* Proposer la lecture de l'article de cet article <https://www.lemonde.fr/pixels/article/2014/10/27/les-petits-secrets-des-photos-numeriques_4512862_4408996.html>
* Diviser la classe en deux groupes : ceux qui vont défendre l'utilisation des données Exif ou de géolocalisation à fins de protection publique ou de service (applications de transports) par exemple et ceux qui vont dénoncer les risque de dérive (atteintes à la vie privée). Chaque groupe a pour consigne d'appuyer ses arguments par des exemples issus d'une recherche d'information.

4. Quatrième temps : Synthèse de cours

Distribution d'un document avec une  partie déjà écrite : définitions et principes (metadonnées EXIF, géolocalisation par GPS ...) et une partie libre rédigée en commun à l'issue  du débat.

## Question 2 :


* Les parcours Pix sont décrits sur cette page <https://dane.ac-lyon.fr/spip/Decouvrir-et-utiliser-les-parcours>
* Le cours de SNT est la rampe de  lancement idéal pour  l'usage de la platforme Pix.  :
  * Une séance en début d'année pour présenter la plateforme et le CRCN. Voir des ressources sur <https://dane.ac-lyon.fr/spip/+-CRCN-PIX-+>.
  * Entretien d'une pratique régulière par des temps de 15 minutes de Pix en fin de séance.
  * On peut éventuellement utiliser des campagnes personnalisées ou thématiques pour évaluer les élèves en fin de thème.


# Sujet 2

## Question 1

Pour des exposés en HTML-CSS sur le thème Web :

* Un exemple de memento sur HTML/CSS : <https://parc-nsi.github.io/premiere-nsi/chapitre2/memo/MemoHTML-CSS-2020.pdf> 
* Des  exemples d'activités  sur HTML/CSS :  [https://frederic-junier.org/SNT/Theme1_Web/SNT_Activite3_Web.html](https://frederic-junier.org/SNT/Theme1_Web/SNT_Activite3_Web.html)
* Exemple de cahier des charges et de sujets sur l'histoire de l'informatique sur <https://frederic-junier.org/NSI/HTML-CSS/site/mini_projet/mini_projet.html>  (login : parc mot de passe : premiereNSI).
* Un exemple de squelette de site à compléter [https://parc-nsi.github.io/premiere-nsi/Projets/HTML-CSS-Histoire/modele.zip](https://parc-nsi.github.io/premiere-nsi/Projets/HTML-CSS-Histoire/modele.zip).
* Pour les exemples de sujets sur l'histoire du Web on pourra s'inspirer de  cette double page du manuel Delagrave , puiser des idées dans cette frise du musée de l'informatique [https://www.computerhistory.org/timeline/networking-the-web/](https://www.computerhistory.org/timeline/networking-the-web/) ou de cet article <https://interstices.info/les-debuts-du-web-sous-loeil-du-w3c/>.

  !["Frise Web"](images/frise_web.png "alt")\

* Un [guide](ressources/FormationCapesNSI-2019-10-21-Seance2.pdf) 
de  construction d'une activité de découverte des langages HTML et CSS.

* Un exemple de Quizz sur le thème Web : <https://www.quiziniere.com/PartageExercice/AREN7N6NR9>

## Question 2

Une activité de SNT sur la capacité du programme « Maîtriser les réglages les plus importants concernant
la gestion des cookies, la sécurité et la confidentialité d’un Paramètres de sécurité d’un navigateur. »

[https://frederic-junier.org/SNT/Theme1_Web/ressources/SNT-MoteursRecherches-DonnesPersonnelles-VersionFormationCapesNSI.pdf](https://frederic-junier.org/SNT/Theme1_Web/ressources/SNT-MoteursRecherches-DonnesPersonnelles-VersionFormationCapesNSI.pdf)


# Sujet 3



## Question 1 :

1. Premier temps :
  * Présenter le contexte  (crise du Covid en octobre 2020, hospitalisation du président Trump, campagne présidentielle)
  * Diffuser d'abord aux élèves les photos du président Trump au travail dans son bureau puis la capture d'écran du tweet
  * Echange oral : demander aux élèves d'analyser ces images et questionner leur connaissance des réseaux sociaux d'information : 
    * quel est le message des images initiales ?
    * quels messages sont diffusés par le réseau Twitter ?
    * comment sont formatés ces messages (sens des différentes icones et statistiques)      
  * Diffuser la video de décryptage :
    * quel est le message de l'auteur du tweet ?
    * qu'est-ce qu'une donnée exif ?



## Questions 2 et 3 :

2. Second temps :

Analyser les données exif d'une image avec un outil. On pourra utiliser l'outil en ligne <http://exif-viewer.com/>    pour éditer les metadonnées de la photo ou le plugin Firefox <https://addons.mozilla.org/fr/firefox/addon/exif-viewer/>  ou [exif-tool](https://exiftool.org/) en version portable.

* Proposer des activités sur le manuel pour découvrir les données Exif : 
 * Activité de découverte comme celle page 111 du manuel Delagrave :  
![données exif](images/exif.png "alt")\
 * Exercice d'application comme l'exercice 6 page 119 du manuel Delagrave :  
   ![données exif2](images/exif2.png "alt")\
* Proposer un challenge proposé également dans Pix :  retrouver la commune où a été prise cette photo : <http://frederic-junier.org/SNT/images/20181230_162625.jpg>
      
3. Troisième temps :

* Construire une activité sur la vérification de l'information en s'inspirant de la fiche d'accompagnement fournie par le site [InfoHunter](https://api.infohunter.education/sites/default/files/fiche_infohunter_lycee.pdf). Le découpage en cinq étapes est intéressant pour mettre en place une démarche critique : 1) On regarde 2) On vote 3) On enquête 4) On revote 5) On retient

Objectif général : Acquérir les bons réflexes pour décrypter l'information et aiguiser son esprit critique.
Objectifs spécifiques :
* Faire l'état de ses connaissances sur un sujet en particulier
* Apprendre à vérifier et croiser les sources d’information pour comprendre et s'approprier la démarche journalistique
* Identifier des sources fiables d'information sur Internet
* Savoir analyser une page web
* Apprendre à décrypter une image
* Comprendre les codes du reportage vidéo


* On pourra aussi s'apuyer sur les outils fournis par le [CLEMI](https://www.clemi.fr/)

* Enfin, dans le cadre du thème "Réseaux sociaux", on pourra demander aux élèves de rédiger en HTML-CSS le texte d'une pétition en ligne pour dénoncer les dérives des bulles informationnelles et les risques de désinformation (en choisissant un thème comme l'insécurité, les élections, le nucléaire, le réchauffement climatique, la 5G ...). On  pourra leur fournir des exemples comme <https://notreinternet.mozfr.org/post/2020/10/arretez-suggestions-groupes-Facebook>


# Sujet 4


## Question 1 :

Voici un exemple d'activité sur ce thème : <https://frederic-junier.org/SNT/Theme1_Web/ressources/SNT-DonnesPersonnelles-2021.pdf>.


A la fin de la première partie, pour introduire le traçage dans le navigateur j'ai diffusé la première partie de la video dont la transcription est proposée dans cet article : <https://www.laquadrature.net/2020/10/05/le-deguisement-des-trackers-par-cname/>.

On pourrait aussi faire travailler les élèves sur les traceurs dans les applications mobiles :

* Diffusion d'une des videos réalisées par [Exodus Privacy](https://reports.exodus-privacy.eu.org/fr/info/trackers/) : 
  * <https://www.youtube.com/watch?v=sv_xF2KIJZE&feature=youtu.be>
  * <https://www.youtube.com/watch?v=sv_xF2KIJZE>
* Utilisation du moteur de recherche d'[Exodus Privacy](https://reports.exodus-privacy.eu.org/fr/info/trackers/) pour déterminer les pisteurs présents dans les applications mobiles les plus utilisées par les élèves.


* D'autres ressources :
  * Carte mentale des outils et pratiques de protection de la vie privée accessible depuis cet article : <https://linc.cnil.fr/une-cartographie-des-outils-et-pratiques-de-protection-de-la-vie-privee>
  * Données personnelles et réseaux sociaux :
    * 31 raisons de quitter Facebook et 10 solutions <https://byebyefacebook.loupbrun.ca/fr/>
    * Filtrer la pub et le traçage des réseaux sociaux avec un Proxy : <https://42l.fr/Service-Nitter>

## Question 2 :

* Rien à voir avec ce qu'on peut demander aux élèves mais on pourra s'exercer sur ce Quizz du journal Le Monde : <https://www.lemonde.fr/pixels/article/2018/01/25/quiz-donnees-personnelles-connaissez-vous-vos-droits_5246798_4408996.html>

* Une proposition de Quizz : <https://www.quiziniere.com/#/PartageExercice/DVDWM397G3>


# Ressources en lignes :

* Manuels de SNT : <http://www.epi.asso.fr/revue/lu/l1906n.htm>
* Plateformes :
    * <https://etabli.tv/>   nouvelle plateforme de Benjamin Sonntag, plutot technique
    * <https://www.internetsanscrainte.fr>
    * <https://www.infohunter.education>
    * <https://www.laquadrature.net/donnees_perso/>
* Données personnelles :
  * "Le prix du gratuit", une conférence très intéressante pour l'enseignant, lien vers la video et transcription sur  <https://www.april.org/le-prix-du-gratuit-emmanuel-revah-rmll-2018>
  [!["Tout est bon dans le data"](images/toutbondata.png "alt")](https://tube.aquilenet.fr/videos/watch/6966e68b-0436-4ee6-8a5e-ce6eaf34bc00)
  * Applications mobiles : <https://reports.exodus-privacy.eu.org/fr/info/next/>
  * Carte mentale des outils et pratiques de protection de la vie privée accessible depuis cet article : <https://linc.cnil.fr/une-cartographie-des-outils-et-pratiques-de-protection-de-la-vie-privee>

# Sitographie (export Workflowy)

SNT

- Ressources en ligne :
  - https://etabli.tv/
  - https://www.internetsanscrainte.fr
  - https://www.infohunter.education
- Manuels :
  - http://www.epi.asso.fr/revue/lu/l1906n.htm
- Présentation du programme :
  - https://view.genial.ly/5ea80e0c4c892d0cff0251b4/presentation-seances-snt-en-classe-de-seconde
- Images :
  - https://interstices.info/tout-ce-que-les-algorithmes-de-traitement-dimages-font-pour-nous/
  - Pixels et Photosites :
  
    Sur le site de David ROCHE sur Pixees, on trouve l'information de la combinaison
    
    https://pixees.fr/informatiquelycee/n_site/snt_photo_capteur.html
    
    On trouve la même information sur le site de Jérome Trinquet
    
    http://www.jerometrinquet.com/2016/10/23/le-capteur-photo-electrique/
    
    1 pixel = 4 photosite
    
    Ou encore sur le portail du lycée La Martinière
    
    http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv20/html/imageHTML/pages/page9.html"
- Objets connectés :
  - Parcours France IOI : https://amazon.quick-pi.org/  et le depot Github  https://github.com/France-ioi/quick-pi
  - Jeu du dinosaure et cellule photoélectrique :
     https://drive.google.com/file/d/16OXsYGNcCHqDn_NDVnKTZNmvhd05rKwn/view
       
    Voilà ce que ça donne en vidéo : https://youtu.be/0-p0ZJ7Gm_o
 
- Impact environnemental du numérique :
  - 5G : 
    - http://gauthierroussilhe.com/fr/projects/controverse-de-la-5g?fbclid=IwAR2Ac53Jk0O4SFkW9XW6o83V6H_Udj_oSyL4AmKNAbJjeVBUXlT7JSBRa7k
    - https://www.lemonde.fr/planete/article/2020/09/19/une-journee-pour-nettoyer-ses-donnees-numeriques_6052811_3244.html
  - Comparaison impact numérique et autres impacts : https://raphael-lemaire.com/2019/11/02/mise-en-perspective-impacts-numerique/
  - https://www.quelleenergie.fr/magazine/actu-environnement/impact-environnemental-mail-57514/
  - https://theshiftproject.org/
  - https://theconversation.com/linquietante-trajectoire-de-la-consommation-energetique-du-numerique-132532
  - https://www.lemondeinformatique.fr/actualites/lire-datacenters-une-efficience-energetique-maitrisee-78278.html
- Données personnelles :
  - Ressources en ligne :
    - Carte mentale des outils et pratiques de protection de la vie privée accessible depuis cet article : <https://linc.cnil.fr/une-cartographie-des-outils-et-pratiques-de-protection-de-la-vie-privee>
    - Internet sans crainte : https://internetsanscrainte.fr/
    - https://video.lqdn.fr/videos/watch/5e5aed81-3340-4082-bcc3-9105ef5d1564
    - https://www.infohunter.education/accueil
    - https://www.april.org/le-prix-du-gratuit-emmanuel-revah-rmll-2018
  - Traçage / Profilage :
    - Applications mobiles :
      - https://reports.exodus-privacy.eu.org/fr/info/next/
      - Les pisteurs en 2 minutes : https://youtu.be/sv_xF2KIJZE
      - Que récoltent les pisteurs : https://www.youtube.com/watch?v=sv_xF2KIJZE
    - Cname Tracking : https://www.laquadrature.net/2020/10/05/le-deguisement-des-trackers-par-cname/
    - Politique :
      - Cambridge Analytica : https://www.clemi.fr/es/ressources/nos-ressources-pedagogiques/ressources-pedagogiques/quand-les-donnees-personnelles-sechappent-laffaire-cambridge-analytica.html
    - Moteurs de recherche :
      - https://www.eff.org/deeplinks/2018/10/privacy-badger-now-fights-more-sneaky-google-tracking
  - Escap Game :
    - https://dane.ac-besancon.fr/connais-moi-echappe-toi-evasion/
  - Monétisation :
    - https://www.franceinter.fr/economie/tadata-l-application-qui-permet-aux-jeunes-de-monnayer-leurs-donnees-personnelles
  - Fuite de données :
    - https://www.lemonde.fr/economie/article/2020/10/11/comment-l-europe-tente-d-enrayer-la-fuite-de-ses-donnees_6055630_3234.html
    - exemple du Bon Coin : https://www.pixeldetracking.com/fr/le-bon-coin-donnees-personnelles-rgpd
  - Publicité :
    - https://linc.cnil.fr/fr/visualiser-le-web-publicitaire-avec-les-fichiers-adstxt-et-sellersjson
  - gafam
    - Tik tok
      - https://www.liberation.fr/planete/2020/08/10/tiktok-est-plus-un-sujet-politique-et-economique-que-de-cybersecurite_1796503
    - https://infographicjournal.com/wp-content/uploads/2020/07/Information-Giant-Tech-Companies-Collecting.jpg
  - Jeu requetes SQL
    - https://members.loria.fr/MDuflot/files/med/enquete.html
    -  : https://gitlab.com/Nat-Faeeria/enquete-data-snt
    - https://mystery.knightlab.com/
  - Web séries/documentaires :
    - https://donottrack-doc.com/fr/intro/
    - https://www.arte.tv/fr/videos/083310-000-A/tous-surveilles-7-milliards-de-suspects/
  - Risques du traçage sanitaire :
    -  https://risques-tracage.fr
    - https://www.lemonde.fr/blog/binaire/2020/04/20/contact-tracing-contre-covid-19/
    - https://www.inria.fr/fr/contact-tracing-bruno-sportisse-pdg-dinria-donne-quelques-elements-pour-mieux-comprendre-les-enjeux
  - Données de santé :
    - Health Data Hub  :
      -   https://peertube.social/videos/watch/ae8469d8-1505-4d55-a921-0bcaf760374d
      - https://www.numerama.com/tech/656229-health-data-hub-cedric-o-prevoit-de-quitter-microsoft-pour-un-prestataire-francais.html#utm_medium=distibuted&utm_source=rss&utm_campaign=656229
      - https://framablog.org/2020/10/09/predation-de-nos-donnees-de-sante-santenathon-ne-lache-pas-laffaire/
      - https://linuxfr.org/news/le-conseil-d-etat-reconnait-que-le-gouvernement-us-peut-acceder-aux-donnees-de-sante-des-francais
    - https://www.numerama.com/politique/632223-confier-le-health-data-hub-a-microsoft-le-peril-pour-les-donnees-de-sante-ne-serait-pas-prouve.html
- Réseaux sociaux :
  - Série dopamine : https://www.arte.tv/fr/videos/RC-017841/dopamine/
  - Episode de la websérie #dans ta gueule  : 
    "https://www.youtube.com/watch?v=G1_ryVCLWoc
    
    
    "
- Plateformes : 
  - http://www.france-ioi.org/confinement/
- Données structurées :
  - http://www.france-ioi.org/confinement/
    "http://www.france-ioi.org/confinement/tutoriel_enseignants.pdf
    
    
    Outil de notation automatique :
    
    https://framagit.org/georgesk/parcours2score/-/blob/master/doc/README.md
    
    
    https://framagit.org/georgesk/parcours2score/-/tree/master/packages"
- Sites profs : 
  - http://monlyceenumerique.fr/snt_seconde/index.html
  - http://munier.jerome.free.fr
  - Sylvain Rouyer : https://lewebpedagogique.com/snt35/
  - Julien Launay :http://icnisnlycee.free.fr/
  - Louis Paternault : https://snt.ababsurdo.fr/prof/
  - Julien Pecoud : https://snt.ababsurdo.fr/
  - Nicolas Toureau :
    - Site :  https://citescolaire-lannemezan.mon-ent-occitanie.fr/disciplines/sciences-numeriques-et-technologie/localisation-cartographie-et-traitement-54619.htm
    - Fiches connaissances : https://citescolaire-lannemezan.mon-ent-occitanie.fr/disciplines/sciences-numeriques-et-technologie/fiches-connaissances-51467.htm
  - Revue de presse : https://annuel.framapad.org/p/revuedepressesnt
- Image :
  - https://exiftool.org/
- Microbit :
  - Bluetooth : 
    - https://support.microbit.org/support/solutions/articles/19000062330-using-the-micro-bit-bluetooth-low-energy-uart-serial-over-bluetooth-
  - Microbit classroom : https://classroom.microbit.org/
- Usages d'internet : 
  - https://datareportal.com/reports/digital-2020-global-digital-overview
- EMI :
  - https://www.clemi.fr/fr/emi_et_programmes.html
  - https://mooc.chatons.org/
- Thème Internet :
  - Internet sans crainte : https://internetsanscrainte.fr/
  - Comment les données circulent sur Internet (Palais découverte ) : https://youtu.be/9P2d4q4bQXI
  - Internet comment ça fonctionne (MOOC Chatons) : https://framatube.org/videos/watch/f06ad0f4-d585-4aa4-a184-91a33ef89081?start=8s
  - En quoi les Gafam nous controlent  (MOOC Chatons) : https://framatube.org/videos/watch/e291b0fc-9a87-4129-99e8-f26958c0e7ef
- Géolocalisation :
  - https://www.lemonde.fr/pixels/article/2020/10/16/covid-19-google-maps-ameliore-son-indicateur-de-frequentation-des-lieux_6056345_4408996.html
  - http://icnisnlycee.free.fr/index.php/43-snt/localisation-cartographie-et-mob
